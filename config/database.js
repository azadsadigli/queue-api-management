const mysql = require('mysql');
require('dotenv').config({path: __dirname + '/../.env'})
const list = {
    host: process.env.db_host,
    user: process.env.db_user,
    password: process.env.db_password,
    database: process.env.db_name,
};
const db = mysql.createConnection(list);
// console.log(list);
db.connect(err => {
    if(err) throw err;
    // console.log("Connected (message from DB)")
})

module.exports = db;

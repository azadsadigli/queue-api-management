var status_codes = require('../../config/status_codes.js');

// console.log(process.env.PORT)
exports.defaultPage = (req, res) => {
    let list = {
        status: status_codes.HTTP_NOT_FOUND,
        message: 'nothing found',
        data: []
    };
    res.send(list)
}
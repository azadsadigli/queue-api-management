const cnf = './../../config/';
// const db = require(cnf + 'database.js');
const status_codes = require(cnf + 'status_codes.js');

// const session = require('express-session');

// console.log(process.env.usecret)


exports.auth = async (req,res,next) => {
    let usecret = req.headers['usecret'],
        ukey = req.headers['ukey'];
    if(usecret !== process.env.usecret || ukey !== process.env.ukey){
        return res.send({
            "status": status_codes.HTTP_UNAUTHORIZED,
            "message": "Not authorized",
            "response": []
            });
    }else{
        next()
    }
}

exports.no_auth = (req,res,next) => {
    if(req.session.email){return res.redirect('/profile');}
    next()
}

const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const fs = require("fs");
const router = require('./routes/base');
require('dotenv').config({path: __dirname + '/.env'})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.use('/',router);

app.listen(process.env.PORT, () => {
  console.log(`Example app listening at http://localhost:${process.env.PORT}`)
})
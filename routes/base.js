const express = require('express');
const router = express.Router();
const {auth,no_auth} = require('../src/middlewares/auth');

const swaggerUi = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');
const swagger_options = {
    customCss: '.swagger-ui .topbar a { display: none }',
    customSiteTitle: "Queue Management API Document",
    customfavIcon: "https://www.wtamu.edu/_files/images/academics/Study%20abroad%20pictures/api%20logo.png"
};
const swaggerOptions = {
  swaggerDefinition: {
    info: {
        version: "1.0.0",
        title: "QM API Document",
        description: "Queue Management API Document",
        contact: {
          name: "Developer"
        },
        servers: ["http://localhost:5000"]
      }
    },
    apis: ['./routes/*.js']
    // [__dirname + "/base.js"]
};

// console.log(__dirname + "/base.js")
  
const swaggerDocs = swaggerJsDoc(swaggerOptions);
console.log(swaggerDocs);
router.use('/api-docs', swaggerUi.serve);
router.get('/api-docs',swaggerUi.setup(swaggerDocs,swagger_options));

// let docPath = '/api-docs';
// router.get(docPath + '/*', ...swaggerUi.serve);
// router.get(docPath, swaggerUi.setup(swaggerDocs, {baseURL: docPath}));


// const cont_path = (name) => {return `./modules/${name}/controllers/${name}_controller`;}
// const authController = require(cont_path('auth'));
const user_controller = require('../src/modules/user/user_controller');
const home_controller = require('../src/controllers/home_controller');
router.get('/', auth, home_controller.defaultPage);

// Routes
/**
 * @swagger
 * /user/login:
 *  post:
 *    description: User login url
 *    responses:
 *      '200':
 *        description: User information returns
 */
router.get('/user/login', auth, user_controller.loginAction);
/**
 * @swagger
 * /user/list:
 *  get:
 *    description: User list
 *    responses:
 *      '200':
 *        description: A successful response
 */
router.get('/user/list', auth, user_controller.userList);



module.exports = router;